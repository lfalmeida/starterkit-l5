<?php

namespace Lfalmeida\Lbase\Exceptions;

/**
 * Class RepositoryException
 * @package Lfalmeida\Lbase\Exceptions
 */
class RepositoryException extends \Exception
{

}